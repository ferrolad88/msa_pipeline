#!/usr/bin/env bash

PARAM=""
SNAKEFILE="workflow/Snakefile"
OUTDIR="data"
# Can be set to "--use-singularity"

if [ -z "$1" ]; then
    SINGULARITY=""
elif [ $1 = "--use-singularity" ]; then
    SINGULARITY="--use-singularity"   
else
    echo "Unrecognized positional argument. Only '--use-singularity' is accepted."
fi

if ! command -v snakemake &> /dev/null; then
    echo "snakemake not available. Exiting ..."
    exit 1
fi

USECONDA=""
# check if mamba is available
if ! command -v mamba &> /dev/null; then
    echo "The mamba package could not be found. Will use conda."
    USECONDA="--conda-frontend conda"
fi

if [ -f "$SNAKEFILE" ]; then
    echo "Found Snakefile: $SNAKEFILE. Executing test analysis."
else 
    echo "Snakefile not found. Expect in: $SNAKEFILE. This script must be executed from the directory /msa_pipeline"
    exit 1
fi

WGET="wget --no-check-certificate"
CURL="curl"

sleep 3s

## Download data
TAIR10="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/735/GCF_000001735.3_TAIR10/GCF_000001735.3_TAIR10_genomic.fna.gz"
TAIR10_gff="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/735/GCF_000001735.3_TAIR10/GCF_000001735.3_TAIR10_genomic.gff.gz"
Ahalleri="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/900/078/215/GCA_900078215.1_Ahal2.2/GCA_900078215.1_Ahal2.2_genomic.fna.gz"
Alyrata_pret="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/900/205/625/GCA_900205625.1_Alyrpet2.2/GCA_900205625.1_Alyrpet2.2_genomic.fna.gz"
Alyrata="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/004/255/GCF_000004255.2_v.1.0/GCF_000004255.2_v.1.0_genomic.fna.gz"


$WGET $TAIR10 -O $OUTDIR/TAIR10.fa.gz 2>/dev/null || $CURL $TAIR10 -o $OUTDIR/TAIR10.fa.gz ; gzip -f -d $OUTDIR/TAIR10.fa.gz
$WGET $TAIR10_gff -O $OUTDIR/TAIR10.gff.gz 2>/dev/null || $CURL $TAIR10_gff -o $OUTDIR/TAIR10.gff.gz; gzip -f -d $OUTDIR/TAIR10.gff.gz
$WGET $Ahalleri -O $OUTDIR/Ahalleri.fa.gz 2>/dev/null || $CURL $Ahalleri -o $OUTDIR/Ahalleri.fa.gz ; gzip -f -d $OUTDIR/Ahalleri.fa.gz
$WGET $Alyrata_pret -O $OUTDIR/Alyrata_pret.fa.gz 2>/dev/null || $CURL $Alyrata_pret -o $OUTDIR/Alyrata_pret.fa.gz ; gzip -f -d $OUTDIR/Alyrata_pret.fa.gz
$WGET $Alyrata -O $OUTDIR/Alyrata.fa.gz 2>/dev/null || $CURL $Alyrata -o $OUTDIR/Alyrata.fa.gz ; gzip -f -d $OUTDIR/Alyrata.fa.gz

if [[ -s "$OUTDIR/TAIR10.fa" ]] && [[ -s "$OUTDIR/TAIR10.gff" ]] && [[ -s "$OUTDIR/Ahalleri.fa" ]] && [[ -s "$OUTDIR/Alyrata_pret.fa" ]] && [[ -s "$OUTDIR/Alyrata.fa" ]]; then
    echo "Downloaded all test data."
else
    echo "Download failed!"
    exit 1
fi

## Run pipeline in three steps
snakemake --use-conda --rerun-incomplete -j 4 $SINGULARITY $USECONDA --configfile .test/arabidopsis_config.yaml -R align
snakemake --use-conda --rerun-incomplete -j 2 $SINGULARITY $USECONDA --configfile .test/arabidopsis_config.yaml -R roast
snakemake --use-conda --rerun-incomplete -j 4 $SINGULARITY $USECONDA --configfile .test/arabidopsis_config.yaml -R call_conservation

## Move results directory
random=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 13)
mv results .test/arabidopsis_results_$random
echo "Completed tests analysis, if there are no errors, check for results in .test/arabidopsis_results_$random"
