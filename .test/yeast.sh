#!/usr/bin/env bash

PARAM=""
SNAKEFILE="workflow/Snakefile"
OUTDIR="data"
# Can be set to "--use-singularity"

if [ -z "$1" ]; then
    SINGULARITY=""
elif [ $1 = "--use-singularity" ]; then
    SINGULARITY="--use-singularity"   
else
    echo "Unrecognized positional argument. Only '--use-singularity' is accepted."
fi

if ! command -v snakemake &> /dev/null; then
    echo "snakemake not available. Exiting ..."
    exit 1
fi
USECONDA=""
# check if mamba is available
if ! command -v mamba &> /dev/null; then
    echo "The mamba package could not be found. Will use conda."
    USECONDA="--conda-frontend conda"
fi



if [ -f "$SNAKEFILE" ]; then
    echo "Found Snakefile: $SNAKEFILE. Executing test analysis."
else 
    echo "Snakefile not found. Expect in: $SNAKEFILE. This script must be executed from the directory /msa_pipeline"
    exit 1
fi

WGET="wget --no-check-certificate"
CURL="curl"

sleep 3s

## Download data
S288C="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/146/045/GCF_000146045.2_R64/GCF_000146045.2_R64_genomic.fna.gz"
YJM993="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/662/435/GCA_000662435.2_Sc_YJM993_v1/GCA_000662435.2_Sc_YJM993_v1_genomic.fna.gz"
ySR128="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/004/328/465/GCA_004328465.1_ASM432846v1/GCA_004328465.1_ASM432846v1_genomic.fna.gz"
BY4742="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/003/086/655/GCA_003086655.1_ASM308665v1/GCA_003086655.1_ASM308665v1_genomic.fna.gz"

$WGET $S288C -O $OUTDIR/S288C.fa.gz 2>/dev/null || $CURL $S288C -o $OUTDIR/S288C.fa.gz ; gzip -f -d $OUTDIR/S288C.fa.gz
$WGET $YJM993 -O $OUTDIR/YJM993.fa.gz 2>/dev/null || $CURL $YJM993 -o $OUTDIR/YJM993.fa.gz ; gzip -f -d $OUTDIR/YJM993.fa.gz
$WGET $ySR128 -O $OUTDIR/ySR128.fa.gz 2>/dev/null || $CURL $ySR128 -o $OUTDIR/ySR128.fa.gz ; gzip -f -d $OUTDIR/ySR128.fa.gz
$WGET $BY4742 -O $OUTDIR/BY4742.fa.gz 2>/dev/null || $CURL $BY4742 -o $OUTDIR/BY4742.fa.gz ; gzip -f -d $OUTDIR/BY4742.fa.gz

if [[ -s "$OUTDIR/S288C.fa" ]] && [[ -s "$OUTDIR/YJM993.fa" ]] && [[ -s "$OUTDIR/ySR128.fa" ]] && [[ -s "$OUTDIR/BY4742.fa" ]]; then
    echo "Downloaded all test data."
else
    echo "Download failed! Check whether wget or curl are available and that hyperlinks are live"
    exit 1
fi

## Run pipeline in three steps

snakemake --use-conda --rerun-incomplete -j 4 $SINGULARITY $USECONDA --configfile .test/yeast_config.yaml -R align
snakemake --use-conda --rerun-incomplete -j 2 $SINGULARITY $USECONDA --configfile .test/yeast_config.yaml -R roast
snakemake --use-conda --rerun-incomplete -j 4 $SINGULARITY $USECONDA --configfile .test/yeast_config.yaml -R call_conservation

## Move results directory
random=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 13)
mv results .test/yeast_results_$random
echo "Completed tests analysis, if there are no errors, check for results in .test/yeast_results_$random"
