# MSA_pipeline

The msa_pipeline bundles existing tools to make multiple alignment of genomes easy. A simplified schema of the pipeline is shown below.

![Scheme](workflow/images/msa_pipeline_scheme.jpg){width=20%}

## Quickstart

For a quickstart with your own data, you can follow the instructions below. We recommend testing the pipeline with our test data first (see section **Testing the pipeline**), to ensure the pipeline will work correctly.

To get started, clone this repository.

```
git clone https://bitbucket.org/bucklerlab/msa_pipeline.git
```

You can now prepare the run with the pipeline by doing the following: 

1. Place your (repeat masked) FASTA sequences suffixed with `.fa` into `msa_pipeline/data`
2. Copy `msa_pipeline/config/config.yaml` to, for example, `msa_pipeline/config/myconfig.yaml` and edit the new config file to include your reference (parameter `refName:`) and query species (parameter `species:`). The reference and query species must match filenames in `msa_pipeline/data` without the `.fa` suffix. 

The pipeline can then be executed from the `/msa_pipeline` directory in two steps as shown below. Note that the `-j` flag controls the number of threads used and should be adjusted based on user requirments.   
  
```
snakemake --use-conda -j 5 --configfile config/myconfig.yaml -R align
snakemake --use-conda -j 1 --configfile config/myconfig.yaml -R roast
```

The practical reason for splitting the pipeline into two steps is that the first pairwise alignment step `align` is thread-intensive and the second multiple alignment step `roast` is single-threaded but memory-intensive and time-intensive. The two steps thus allow adjustment of computational resources, e.g. when submitting the pipeline as single scheduled jobs.

A final optional step will generate GERP and phyloP conservation scores based on the previously generated multiple alignment.

```
snakemake --use-conda -j 5 --configfile config/myconfig.yaml -R call_conservation
```

For more information on the output files, see the below section on **Explanation of output files**.

## Testing the pipeline

To test the pipeline before running on your own data, you can align some simulated mammalian sequences. This run should complete in <5 min on a desktop computer and uses 1 thread.

```
cd msa_pipeline
bash .test/mammals.sh
```

All output files are written to `.test/mammals_results_{RANDOM_STR}` and the main multiple alignment output file is written to `.test/mammals_results_{RANDOM_STR}/roast/roast.maf`.

A larger set of yeast and plant genomes can also be used to test the pipeline and the parallelization efficiency. By default these test runs use 4 threads and should run in <1h on a standard laptop.

```
bash .test/yeast.sh
bash .test/arabidopsis.sh
```

The config files for each of the three test runs are written to show different ways of parametrising the pipeline. 

```
.test/mammals_config.yaml # Use last without splitting input fasta files
.test/yeast_config.yaml # Split input fasta files for improved parallelization of last
.test/arabidopsis_config.yaml # Use minimap2 aligner for intraspecies alignment
```

### Choosing and tuning the aligner

We recommend the LAST aligner for interspecies alignment. The `msa_pipeline` can also be executed using the generally faster aligner GSAlign and minimap2 (see **Testing the pipeline** above). However, in our experience the default parameters of these aligners are not suitable for interspecies alignments.

The selected aligner and the alignment parameters can be modified in the config file. For example, to run a LAST alignment using the HOXD70 matrix with a custom penalty setting the line could be changed to:

```
lastParams: "-j 3 -u 1 -m50 -p HOXD70 -a 700 -b 20 -e 5000 -d 3500"
```

For more details on LAST parameters, see the official [LAST documentation](https://gitlab.com/mcfrith/last).


### Running the pipeline using a container

In some cases it may be useful to execute the pipeline using a singularity container rather than locally installing the conda environments. To do this, users only need to add the `--use-singularity` flag to their snakemake command. For example:

```
snakemake --use-conda -p -j 4 --use-singularity --configfile config/myconfig.yaml -R align
```

To test whether singularity is working correctly, the test analyses described above can all also be executed using singularity. To do this, execute the bash wrapper as above with the `--use-singularity` flag as an argument.

```
bash .test/mammals.sh --use-singularity 
```

### Software requirements
   
Snakemake is required to run this pipeline and we recommend snakemake version 6.0.0 or higher. The recommended installation is shown below. For more details see [snakemake installation guide](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html).

```
conda install -n base -c conda-forge mamba
conda activate base
mamba create -c conda-forge -c bioconda -n snakemake snakemake
```

Note that the installation should use the exact commands above, including the exact channel priority, otherwise snakemake may be improperly installed.

To run the pipeline using a prebuilt singularity container, you must have singularity installed on your system. Singularity is installed on many HPC systems and can be used without root privileges. However, note that installation of singularity does require root privileges. If you want to install singularity and have these privileges, you can find up-to-date instructions on how to do so in the official [singularity installation guide](https://github.com/sylabs/singularity/blob/master/INSTALL.md).

### Explanation of output files

Results of each `msa_pipeline` run are written to the `/results` directory, which contains several subdirectories with outputs from different steps of the pipeline. The file tree below shows typical outputs of the pipeline. The key output files for most users are likely the final multiple alignment `roast/roast.maf` and the directories `phylop` and `gerp`.

```bash
.
`-- results
    |-- conservation_raw # raw phylop/gerp outputs
    |-- genome # fasta indexes
    |-- gerp # GERP conservation scores
    |   |-- chr1.gerp.final.bed
    |   |-- chr2.gerp.final.bed
    |   |-- chr3.gerp.final.bed
    |   |-- chr4.gerp.final.bed
    |   `-- chr5.gerp.final.bed
    |-- phylop # phyloP conservation scores
    |   |-- chr1.phylop.final.bed
    |   |-- chr2.phylop.final.bed
    |   |-- chr3.phylop.final.bed
    |   |-- chr4.phylop.final.bed
    |   `-- chr5.phylop.final.bed
    |-- psl # query alignments in PSL format
    |   |-- Ahalleri.psl
    |   |-- Alyrata.psl
    |   `-- Alyrata_pret.psl
    |-- roast # multiple alignment output
    |   `-- roast.maf
    |-- toast # pairwise MAF alignments
    |   |-- TAIR10.Ahalleri.toast2.maf
    |   |-- TAIR10.Alyrata.toast2.maf
    |   `-- TAIR10.Alyrata_pret.toast2.maf
    `-- tree
        |-- neutral.mod # neutral model inferred by phylofit
        |-- neutral.tre # tree with branch lengths based on neutral model
        |-- raxml # RAxML output
        |-- sitefold.bed # cds_fold.py output of 4-fold degenerate sites
        `-- topology.nwk # taxa topology based on user input or mashtree
```

It is worth noting that the phyloP output scores are -log p-values computed using a likelihood ratio test and the CONACC method for detecting conserved and accelerated sites (see [phyloP Help](http://compgen.cshl.edu/phast/help-pages/phyloP.txt)). Sites under acceleration are flagged by making their p-values negative. The two columns of GERP scores show the Expected substitutions under neutrality and the 'Rejected Substitutions' based on the observed aligned nucleotides (see [GERP Help](http://mendel.stanford.edu/SidowLab/downloads/gerp/index.html)).

### Calling conservation for previously generated alignments

In some cases, a multiple alignment in MAF format may already be available and the aim is simply to generation conservation scores for this alignment. Although this requires some brief set up to 'trick' snakemake into thinking the previous rules in the pipeline have been executed, it is straightforward. 

1. The `msa_pipeline/data` directory should contain one FASTA file for each taxon in the alignment. These files won't be used and can be empty dummy files.
2. An appropriate config file such as `config/myconfig.yaml` must exist, containing the names of query and reference taxa. Optionally a GFF file for the reference can be designated if the neutral tree is to be calculated based on 4d sites. Other parameters in the config will be ignored for conservation calling.

Once a config file and (dummy) FASTA files have been created, the previously generated alignment can be moved and renamed into the expected directory structure.

```
# create the output directories manually
mkdir msa_pipeline/results
mkdir msa_pipeline/results/roast
# move the previously generated alignment into the output directory and rename
mv /my/file/path/alignment.maf msa_pipeline/results/roast/roast.maf
# ensure the input file is 'new' to avoid rerunning previous pipeline rules
touch msa_pipeline/results/roast/roast.maf
```

Now the conservation scores can be calculated with the below command from the `msa_pipeline` directory.

```
snakemake --use-conda -j 1 --configfile config/myconfig.yaml -R call_conservation
```

Note than you can use the `-n` flag with snakemake to first check which rules will be executed. This can help ensure only the rules relevant for conservation calling are run. If snakemake is trying to execute the `align` and `roast` steps, ensure that the `roast.maf` has been made available as described above.


## Troubleshooting

### Common errors

#### Input file errors
When the snakemake run terminates with an error despite snakemake (version > 6.0.0) being correctly installed, there are several common causes related to input files:

- Input FASTA files in the /data directory do not match samples listed in the config file parameters `species` and `refName`
- Input FASTA files have chromosomes/scaffolds with special characters; ideally, we want names consisting of only alphanumeric characters and "_"
- When providing a GFF file to calculate a neutral model from fourfold degenerate sites, the GFF chromosome/scaffold names do not match the reference FASTA
- Check the optional custom parameters in the config file and ensure that they are valid for the respective aligner

You can check the log files specific to each rule for other potential sources of error in the directory `/logs`. 

#### Conda-related errors

In some HPC environments I have also come across errors when a $PYTHONPATH variable is set, which can lead to wires getting crossed and incompatibilities between python version. Conda does not use $PYTHONPATH so these issues can be resolved by unsetting it.
```
unset PYTHONPATH
```
For further issues related to conda environments, a solution may be to run the pipeline using the singularity container (see section above 'Running the pipeline using a container'). 

### Conservation calling errors

The optional conservation calling step of the pipeline requires inference of a neutral evolutionary model using a collection of >50 sites in the genome. When providing a GFF file to use 4-fold degenerate coding sites to calculate the neutral model, this means >50 of these sites need to be in the alignment and be covered in >90% of samples. If there are insufficient sites in the alignment, the conservation calling step will not produce outputs.

To increase the number of sites available when aligned diverged genomes, parameter tuning of the aligner may be required. In practice this may involve running the pipeline with a range of different alignment scoring parameters to find an optimal setting for a specific set of genomes. An example of this is described in the `msa_pipeline` manuscript (see section **How to cite**).

### Patching netToAxt for large, fragmented alignments

For diverged large plant genomes, it is common that large numbers of alignment chains are produced, which can lead to to `netToAxt` error: `chainId 273107809, can only handle up to 268435456`. Currently the only way to address this issue is to patch and recompile the utility and then to hardcode the path into the relevant snakemake rule net_to_axt_to_maf which can be found in `workflow/rules/chain_and_net.smk`. The script below compiles a patched netToAxt in `$(pwd)/bin`. 

```
wget http://hgdownload.cse.ucsc.edu/admin/exe/userApps.archive/userApps.v421.src.tgz
tar -xvzf userApps.v421.src.tgz
export MACHTYPE=x86_64
export BINDIR=$(pwd)/bin
export L="${LDFLAGS}"
mkdir -p "$BINDIR"
(cd userApps/kent/src/lib && make)
(cd userApps/kent/src/htslib && make)
(cd userApps/kent/src/jkOwnLib && make)
(cd userApps/kent/src/hg/lib && make)
(cd userApps/kent/src/hg/mouseStuff/netToAxt &&  sed -i 's/maxChainId (256\*1024\*1024)/maxChainId (256\*1024\*1024\*6)/' netToAxt.c && make)
chmod +x $BINDIR/netToAxt
```

### How to cite

If you use msa_pipeline in your work, please cite:

> Yaoyao Wu, Lynn Johnson, Baoxing Song, Cinta Romay, Michelle Stitzer, Adam Siepel, Edward Buckler, Armin Scheben (2021)
> A multiple genome alignment workflow shows the impact of repeat masking and parameter tuning on alignment of functional regions in plants  
> *bioRxiv* 2021.06.01.446647. [doi:10.1101/2021.06.01.446647](https://doi.org/10.1101/2021.06.01.446647)

